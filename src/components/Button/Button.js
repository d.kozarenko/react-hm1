import React, { Component } from "react";
import styled from "styled-components";

const StyledButton = styled.button`
  background-color: ${({ bgColor }) => bgColor || "lightblue"};
  border: 1px solid transparent;
  padding: ${({ padding }) => padding || "0px"};
  margin: ${({ margin }) => margin || "0"};
  font-size: ${({ fontSize }) => fontSize || "15px"};
  font-family: ${({ fontFamily }) => fontFamily || "sans-serif"};
  border-radius: 3px;
  color: white;
  cursor: pointer;
`;

export default class Button extends Component {
  render() {
    const { text, handleClick, id } = this.props;
    return (
      <StyledButton id={id} {...this.props} onClick={handleClick}>
        {text}
      </StyledButton>
    );
  }
}
