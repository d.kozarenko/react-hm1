import React, { Component } from "react";
import "../Button/Button.js";
import styled from "styled-components";
import Button from "../Button/Button.js";

const ModalHeader = styled.div`
  display: flex;
  align-items: center;
  background-color: ${({ bgHeaderColor }) => bgHeaderColor || "#d44637"};
`;

const ModalContent = styled.div`
  display: inline-block;
  background-color: ${({ bgColor }) => bgColor || "red"};
  max-width: 525px;
  border-radius: 3px;
`;

const ModalWrapper = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: rgba(0, 0, 0, 0.4);
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ModalTitle = styled.h3`
  display: inline-block;
  margin: 0;
  font-size: 22px;
  color: white;
  padding: 23px 120px 21px 31px;
  border-radius: 3px;
`;

export default class Modal extends Component {
  componentDidMount() {
    document.addEventListener("click", this.modalHandler);
  }
  componentWillUnmount() {
    document.removeEventListener("click", this.modalHandler);
  }

  modalHandler = (e) => {
    const modal = document.getElementById(`${this.props.id}`);
    const first_button = document.getElementById("first_button");
    const second_button = document.getElementById("second_button");
    if (
      !modal.contains(e.target) &&
      e.target !== first_button &&
      e.target !== second_button
    ) {
      this.props.handleClick();
    }
  };
  render() {
    if (!this.props.show) {
      return null;
    }

    const { title, handleClick, bgHeaderColor, closeBtn, text, btns, id } =
      this.props;
    return (
      <ModalWrapper>
        <ModalContent id={id} {...this.props}>
          <ModalHeader bgHeaderColor={bgHeaderColor}>
            <ModalTitle>{title}</ModalTitle>
            {closeBtn && (
              <Button
                bgColor={"transparent"}
                text={"Х"}
                fontSize={"19px"}
                handleClick={handleClick}
              ></Button>
            )}
          </ModalHeader>
          {text}
          {btns}
        </ModalContent>
      </ModalWrapper>
    );
  }
}
