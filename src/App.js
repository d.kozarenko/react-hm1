import Modal from "./components/Modal/Modal.js";
import Button from "./components/Button/Button.js";
import React from "react";
import styled from "styled-components";

const AppWrapper = styled.div`
  margin-top: 100px;
  text-align: center;
  width: 100%;
  height: 100%;
`;

const ModalText = styled.p`
  padding: 38px 40px;
  line-height: 28px;
  font-size: 15px;
  color: white;
  margin: 0;
  text-align: center;
`;

const BtnsWrapper = styled.div`
  padding: 0 145px 24px;
`;

class App extends React.Component {
  state = {
    showModal1: false,
    showModal2: false,
  };
  firstModalSwap = () => {
    this.setState({
      showModal1: !this.state.showModal1,
    });
  };
  secondModalSwap = () => {
    this.setState({
      showModal2: !this.state.showModal2,
    });
  };
  createBtns = (
    text1,
    text2,
    bgColor1,
    bgColor2,
    padding1,
    padding2,
    handleClick
  ) => {
    return (
      <BtnsWrapper>
        <Button
          padding={padding1}
          bgColor={bgColor1}
          margin={"0 15px 0 0"}
          text={text1}
          handleClick={handleClick}
        />
        <Button
          padding={padding2}
          bgColor={bgColor2}
          text={text2}
          handleClick={handleClick}
        />
      </BtnsWrapper>
    );
  };

  render() {
    const firstModalBtns = this.createBtns(
      "Ok",
      "Cancel",
      "#B3382C",
      "#B3382C",
      "11px 41px 14px",
      "11px 28px 14px",
      this.firstModalSwap
    );

    const secondModalBtns = this.createBtns(
      "1",
      "2",
      "#5AC18E",
      "#5AC18E",
      "11px 20px 14px",
      "11px 20px 14px",
      this.secondModalSwap
    );

    const firstModal = (
      <Modal
        title={"Do you want to delete this file?"}
        closeBtn={true}
        show={this.state.showModal1}
        id="first_modal"
        handleClick={this.firstModalSwap}
        btns={firstModalBtns}
        text={
          <ModalText>
            Once you delete this file, it won’t be possible to undo this action.{" "}
            <br /> Are you sure you want to delete it?
          </ModalText>
        }
      />
    );
    const secondModal = (
      <Modal
        title={"Условия пользования"}
        closeBtn={false}
        show={this.state.showModal2}
        id="second_modal"
        handleClick={this.secondModalSwap}
        btns={secondModalBtns}
        bgColor={"green"}
        bgHeaderColor={"lightgreen"}
        text={
          <ModalText>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </ModalText>
        }
      />
    );

    return (
      <>
        <AppWrapper>
          <Button
            bgColor={"red"}
            padding={"10px"}
            margin={"0 20px 0 0"}
            text={"First modal btn"}
            id={"first_button"}
            handleClick={this.firstModalSwap}
          />
          <Button
            bgColor={"green"}
            padding={"10px"}
            text={"Second modal btn"}
            id={"second_button"}
            handleClick={this.secondModalSwap}
          />
          {this.state.showModal1 && firstModal}
          {this.state.showModal2 && secondModal}
        </AppWrapper>
      </>
    );
  }
}

export default App;
